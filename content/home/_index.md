---
title: eclipse.org
headline: The Community for Open Innovation and Collaboration
tagline: The Eclipse Foundation provides our global community of individuals and
  organizations with a mature, scalable, and business-friendly environment for
  open source software collaboration and innovation.
hide_page_title: true
hide_sidebar: true
hide_breadcrumb: true
show_featured_story: true
date: 2004-02-02T18:54:43.927Z
layout: single
links:
  - - href: /projects/
    - text: Discover Projects
  - - href: /collaborations
    - text: Industry Collaborations
  - - href: /membership/
    - text: Members
  - - href: /org/value
    - text: Business Value
container: container-fluid
lastmod: 2022-04-29T14:50:37.859Z
header_wrapper_class: header-wrapper header-anniversary-bg-img header-dark-bg-img
  featured-jumbotron-bottom-shape
description: The Eclipse Foundation provides our global community of individuals and
  organizations with a mature, scalable, and business-friendly environment for
  open source software collaboration and innovation.
keywords:
  - eclipse
  - project
  - plug-ins
  - plugins
  - java
  - ide
  - swt
  - refactoring
  - free java ide
  - tools
  - platform
  - open source
  - development environment
  - development
  - ide
  - eclipse foundation
---

{{< pages/home/newsroom_ads publishTo="eclipse_org_home" adFormat="ads_top_leaderboard" >}}

{{< grid/section-container class="margin-bottom-30 margin-top-50 news-list" >}}

  {{< grid/div class="col-lg-10 col-lg-offset-2 col-md-12 padding-bottom-50" isMarkdown="false" >}}

    <div class="block-summary-title">
      <i class="fa fa-comments fa-3x center-block brand-primary text-center padding-bottom-10" aria-hidden="true"></i>
      <h2>Announcements</h2>
    </div>

    {{< newsroom/news id="news-list-announcements" paginate="0" type="announcements" publishTarget="eclipse_org" count="4"  >}}

    <ul class="list-inline text-center">
      <li><a class="btn btn-primary" href="https://newsroom.eclipse.org/rss/news/eclipse_org/announcements.xml" title="Subscribe to our RSS-feed"><i class="fa fa-rss margin-right-5"></i> Subscribe </a></li>
      <li><a class="btn btn-primary" href="https://newsroom.eclipse.org/eclipse/announcements/"> View all </a></li>
    </ul>

  {{</ grid/div >}}

  {{< grid/div class="col-lg-10 col-md-12 padding-bottom-50" isMarkdown="false" >}}
    <div class="block-summary-title">
      <i class="fa fa-users fa-3x center-block brand-primary text-center padding-bottom-10" aria-hidden="true"></i>
      <h2>Community News</h2>
    </div>

    {{< newsroom/news id="news-list-community" paginate="0" type="community_news" publishTarget="eclipse_org" count="4" >}}

    <ul class="list-inline text-center">
      <li><a class="btn btn-primary" href="https://newsroom.eclipse.org/rss/news/eclipse_org/community-news.xml" title="Subscribe to our RSS-feed"><i class="fa fa-rss margin-right-5"></i> Subscribe </a></li>
      <li><a class="btn btn-primary" href="https://newsroom.eclipse.org/eclipse/community-news/"> View all </a></li>
    </ul>

  {{</ grid/div >}}
{{</ grid/section-container >}}

{{< pages/home/newsroom_ads publishTo="eclipse_org_home" adFormat="ads_leaderboard" >}}

{{< pages/home/featured-committer-project >}}

{{< pages/home/foundation-by-the-numbers >}}
