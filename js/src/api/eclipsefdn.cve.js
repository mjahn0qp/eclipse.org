/*!
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

const apiPath = `https://api.eclipse.org/cve`;

const cveMapper = data => ({
    id: data.id,
    datePublished: data.date_published,
    status: data.status,
    summary: data.summary,
    project: data.project,
    cvss: data.cvss,
    liveLink: data.live_link,
    cvePullRequest: data.cve_pull_request,
    nvdLink: `https://nvd.nist.gov/vuln/detail/${data.id}`,
});

const getAllCVEs = async () => {
    try { 
        const response = await fetch(apiPath);
        if (!response.ok) throw new Error('Problem with the request to CVE API');

        const data = await response.json();
        if (!Array.isArray(data)) throw new TypeError('Expected an array as CVE API response');

        const cves = data
          .map(cveMapper)
          .filter(cve => cve.status === 'PUBLIC');

        return [cves, null];
    } catch (error) {
        return [null, error];
    }
}

export default getAllCVEs;
